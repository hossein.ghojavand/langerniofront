import React, {useEffect, useState, Suspense} from 'react';
import {BrowserRouter} from 'react-router-dom';
import {Switch} from "react-router-dom";
import {renderRoutes, routes} from "./routes";
import Layout from "../components/layout";
 
function App() {
    const [renderedRoutes, setRenderedRoutes] = useState();

    useEffect(() => {
        let result = renderRoutes(routes);
        setRenderedRoutes(result);
    }, []);

    return (
        <BrowserRouter>
            <Layout>
                <Suspense fallback={() => <h1>Loading...</h1>}>
                    <Switch>{renderedRoutes}</Switch>
                </Suspense>
            </Layout>
        </BrowserRouter>
    );
}

export default App;
