import { lazy } from "react";
import { Route } from "react-router-dom";

const Landing = lazy(() => import("../pages/landing/index"));
const SignIn = lazy(() => import("../pages/signIn/index"));
const SignUp = lazy(() => import("../pages/signUp/index"));
const Tree = lazy(() => import("../pages/tree/index"));
const Profile = lazy(() => import("../pages/profile/index"));

export const routes = [
  {
    exact: true,
    path: "/",
    component: Landing,
  },
  {
    path: "/signIn",
    component: SignIn,
  },
  {
    path: "/signUp",
    component: SignUp,
  },
  {
    path: "/tree",
    component: Tree,
  },
  {
    path: "/profile",
    component: Profile,
  }
];

export const renderRoutes = (routes) => {
  return routes.map((route, key) => {
    return (
      <Route key={key} path={route.path} exact={route.exact}>
        <route.component />
      </Route>
    );
  });
};
