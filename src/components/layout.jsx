import React, {useEffect} from 'react';
import {connect} from 'react-redux';
import { ToastContainer, toast } from 'react-toastify';
import Axios from 'axios';
import {urls} from "../utils/urls";
import Cookies from "universal-cookie";

// import Material UI components
import Grid from "@material-ui/core/Grid";
import { makeStyles } from "@material-ui/core/styles";
import NavBar from "./navBar";
import Footer from "./footer";
import '../assets/style/layout.css';
import 'react-toastify/dist/ReactToastify.css';

const useStyles = makeStyles((theme) => ({
    pagesContainer: {
        minHeight: "calc(100vh - 8rem)",
        marginTop: "4rem",
    }
}));

const Layout = ({children, dispatch, user}) => {

    const classes = useStyles();
    const cookies = new Cookies();
    const token = cookies.get('token');

    useEffect(() => {
        if (token && !user.isAuthenticated) {
            dispatch({
                type: 'LOGIN'
            })
        }
    }, []);

    useEffect(() => {
        if (user.isAuthenticated){
            Axios.get(urls.profile, {
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${token}`
                }
            }).then(res => {
                dispatch({
                    type: 'SET-USER',
                    data: {
                        userName: res.data?.data?.name,
                        userEmail: res.data?.data?.email,
                    }
                })
            }).catch(err => {
                toast.error(err.message);
            })
        }
    }, [user.isAuthenticated]);

    return (
        <div className='layout'>
            <Grid container>
                <Grid item xs={12}>
                    <NavBar />
                </Grid>
                <Grid item xs={12} className={classes.pagesContainer}>
                    {children}
                </Grid>
                <Grid item xs={12}>
                    <Footer/>
                </Grid>
            </Grid>
            <ToastContainer
                position="top-center"/>
        </div>
    )
};

const mapStateToProps = state => ({
    user: state.user,
});

export default connect(mapStateToProps)(Layout);