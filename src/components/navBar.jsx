import React, {useState, Fragment} from 'react';
import {Link as LinkRouter, useHistory} from 'react-router-dom';
import {connect} from "react-redux";
import Cookies from "universal-cookie";

// import material ui components
import {makeStyles} from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import MenuIcon from '@material-ui/icons/Menu';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import AccountCircleOutlinedIcon from '@material-ui/icons/AccountCircleOutlined';
import ExitToAppOutlinedIcon from '@material-ui/icons/ExitToAppOutlined';
import HomeOutlinedIcon from '@material-ui/icons/HomeOutlined';

const useStyles = makeStyles((theme) => ({
    root: {
        flexGrow: 1,
        zIndex: theme.zIndex.drawer + 1,
        position: 'sticky',
        top: 0,
    },
    title: {
        flexGrow: 1,
        textAlign: 'center',
        fontWeight: 'bold',
        fontSize: '1.5rem'
    },
    menuButton: {
        display: 'flex',
        justifyContent: 'flex-start',
        width: '7rem'
    },
    userNav: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'center',
        width: '7rem'
    },
    line: {
        borderLeft: '1px solid #fff',
        height: '1.3rem',
        margin: '0 0.3rem',
    },
    button: {
        minWidth: 'inherit',
    },
    sideBar: {
        direction: 'rtl',
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'center',
        width: '18vw',
        minWidth: '13rem',
        color: 'white',
    },
    sideBarPaper: {
        backgroundColor: theme.palette.primary.dark,
    },
    userSidebar: {
        display: 'flex',
        justifyContent: 'space-around',
        alignItems: 'center',
        margin: '0',
    },
    userName: {
        fontWeight: 'bold',
        margin: '0',
    },
    userIcon: {
        fontSize: '7rem'
    },
    sideBarList: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'flex-end',
    },
    listItemIcon: {
        minWidth: 'inherit',
    },
    listItemText: {
        marginLeft: '1rem',
    }
}));


const NavBar = ({user}) => {

    const classes = useStyles();
    const [showSideBar, setShowSideBar] = useState(false);
    const cookies = new Cookies();
    const history = useHistory();

    const exitHandler = () => {
        cookies.remove('token');
        if(history.location.pathname === '/'){
            window.location.reload();
        }
        else {
            history.push('/');
        }
    };

    return (
        <div className={classes.root} >
            <AppBar>
                <Toolbar style={{color: '#fff'}}>
                    <div className={classes.menuButton}>
                        <IconButton edge="start"  onClick={() => setShowSideBar(true)}
                                    color="inherit" aria-label="menu">
                            <MenuIcon/>
                        </IconButton>
                    </div>
                    <Typography variant="h6" className={classes.title}>
                        <Link to='/' component={LinkRouter} color="inherit" style={{textAlign: 'center'}}>
                            لنگرنیو
                        </Link>
                    </Typography>
                    <div className={classes.userNav}>
                        {
                            user.isAuthenticated ?
                                null
                                :
                                <Fragment>
                                    <Link to='/signIn' component={LinkRouter} color="inherit">
                                        <Button className={classes.button} color="inherit">ورود</Button>
                                    </Link>

                                    <div className={classes.line}/>
                                    <Link to='/signUp' component={LinkRouter} color="inherit">
                                        <Button className={classes.button} color="inherit">ثبت نام</Button>
                                    </Link>
                                </Fragment>
                        }
                    </div>
                </Toolbar>
            </AppBar>

            {/* sideBar */}
            <Drawer anchor='left' open={showSideBar} PaperProps={{className: classes.sideBarPaper}}
                    onClose={() => setShowSideBar(false)}>
                <div
                    className={classes.sideBar}
                    role="presentation"
                    onClick={() => setShowSideBar(false)}
                    onKeyDown={() => setShowSideBar(false)}
                >
                    <div className={classes.userSidebar}>
                        <p className={classes.userName}>{user.userName}</p>
                        <AccountCircleOutlinedIcon className={classes.userIcon}/>
                    </div>

                    <Link to='/profile' component={LinkRouter} color="inherit" style={{textAlign: 'center'}}>
                        <Button className={classes.button} color="inherit">مشاهده پروفایل</Button>
                    </Link>

                    <Divider/>
                    <List>
                        <ListItem button>
                            <ListItemText primary='خانه' className={classes.listItemText}/>
                            <ListItemIcon className={classes.listItemIcon}>
                                <HomeOutlinedIcon style={{color: '#fff'}}/>
                            </ListItemIcon>
                        </ListItem>
                        {
                            user.isAuthenticated ?
                                <ListItem button>
                                    <ListItemText primary='خروج' className={classes.listItemText} onClick={exitHandler}/>
                                    <ListItemIcon className={classes.listItemIcon}>
                                        <ExitToAppOutlinedIcon style={{color: '#fff'}}/>
                                    </ListItemIcon>
                                </ListItem>
                                :
                                null
                        }
                    </List>
                </div>
            </Drawer>
        </div>
    )
};

const mapStateToProps = state => ({
    user: state.user
});

export default connect(mapStateToProps)(NavBar);