import React, { useEffect, useState } from "react";
import {connect} from "react-redux";
import Axios from "axios";
import Cookies from "universal-cookie";
import { toast } from "react-toastify";
import { makeStyles } from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {urls} from "../../../utils/urls";

const useStyles = makeStyles((theme) => ({
  modalBody: {
    position: "absolute",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    width: "40%",
    height: "fit-content",
    backgroundColor: theme.palette.background.paper,
    border: "none",
    borderRadius: theme.spacing(2),
    outline: "none",
    padding: theme.spacing(2),
    top: "50%",
    left: "50%",
    transform: `translate(-50%, -50%)`,
  },
  titleInput: {
    direction: 'ltr',
    marginBottom: theme.spacing(2),
    width: "100%",
  },
  descriptionInput: {
    direction: 'ltr',
    marginBottom: theme.spacing(2),
    width: "100%",
  },
  buttonsDiv: {
    display: "flex",
    justifyContent: "space-around",
    width: "50%",
  },
}));

const CreateTreeModal = ({dispatch, isOpen, handleClose}) => {

  const classes = useStyles();
  const cookies = new Cookies();
  const token = cookies.get('token');
  const [treeTitle, setTreeTitle] = useState('');
  const [treeDes, setTreeDes] = useState('');

  const submitHandler = event => {
    event.preventDefault();
    const data = {
      title: treeTitle,
      description: treeDes,
    };
    Axios.post(urls.createTree, data, {
      headers: {
        Accept: 'application/json',
        Authorization: `Bearer ${token}`
      }
    }).then( res => {
      dispatch({
        type: 'ADD-MY-TREES',
        data: {
          tree: {
            title: res.data?.data?.title,
            id: res.data?.data?.id,
            isAdmin: true,
          }
        }
      });
      handleClose();
      toast.success('شجره نامه با موفقیت ایجاد شد')
    }).catch(err => {
      toast.error('ساخت شجره نامه با مشکل مواجه شد');
    })
  };

  return (
    <Modal
      open={isOpen}
      onClose={handleClose}
      aria-labelledby="simple-modal-title"
      aria-describedby="simple-modal-description"
    >
      <form className={classes.modalBody} onSubmit={submitHandler}>
        <TextField
          className={classes.titleInput}
          required
          id="filled-required"
          label="عنوان"
          value={treeTitle}
          onChange={(e) => setTreeTitle(e.target.value)}
        />
        <TextField
          className={classes.descriptionInput}
          id="outlined-multiline-static"
          required
          label="توضیحات"
          multiline
          rows={9}
          variant="outlined"
          value={treeDes}
          onChange={(e) => setTreeDes(e.target.value)}
        />
        <div className={classes.buttonsDiv}>
          <Button variant="contained" type="submit" color="primary">
            ایجاد
          </Button>
        </div>
      </form>
    </Modal>
  );
};

const mapStateToProps = state => ({
  tree: state.tree
});

export default connect(mapStateToProps)(CreateTreeModal);
