import React from 'react';
import {Link as LinkRouter} from 'react-router-dom';

// import Material UI components
import {makeStyles} from "@material-ui/core/styles";
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Typography from '@material-ui/core/Typography';
import Link from "@material-ui/core/Link";

// import image src
import treeImage from '../../../assets/img/family-tree6.jpeg';

const useStyles = makeStyles((theme) => ({
    cardContainer: {
        boxSizing: 'border-box',
        boxShadow: 'none',
        padding : theme.spacing(2)
    },
    cardContent: {
        boxShadow: '0 0 5px #555',
        borderRadius: '1rem',
        padding: theme.spacing(2),
        color: theme.palette.darkGray,
    },
    linkCard: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        textDecoration: 'none',
    },
    imageCard: {
        width: '60%',
        height: '60%',
        boxShadow: '0 0 5px #555',
        borderRadius: '100%'
    },
    userType: {
        textAlign: 'center'
    }
}));

const TreeCard = ({image, title, isMine, isAdmin, id}) => {

    const classes = useStyles();

    return (
        <Card className={classes.cardContainer}>
            <CardActionArea className={classes.cardContent}>
                <Link
                    to='/tree'
                    component={LinkRouter}
                    color="inherit"
                    onClick={() => localStorage.setItem('treeId', id)}
                    className={classes.linkCard}>
                    <CardMedia
                        className={classes.imageCard}
                        component="img"
                        alt="Contemplative Reptile"
                        height="140"
                        image={treeImage}
                        title="Contemplative Reptile"
                    />

                    <CardContent style={{padding: '0', marginTop: '1rem'}}>
                        <Typography gutterBottom variant="h6" component="h4">
                            {title}
                        </Typography>
                        {
                            isMine ?
                                <Typography gutterBottom className={classes.userType} color="secondary">
                                    {
                                        isAdmin ?
                                            'سرپرست'
                                            :
                                            'کاربر عادی'
                                    }
                                </Typography>
                                :
                                null
                        }
                    </CardContent>
                </Link>
            </CardActionArea>
        </Card>
    )
};

export default TreeCard;