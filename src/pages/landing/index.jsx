import React, { useState, useEffect, Fragment } from 'react';
import {connect} from "react-redux";
import Axios from 'axios';
import {urls} from "../../utils/urls";
import Cookies from "universal-cookie";
import { toast } from 'react-toastify';
import {makeStyles} from "@material-ui/core/styles";
import TreeCard from "./components/treeCard";
import Grid from "@material-ui/core/Grid";
import Button from "@material-ui/core/Button";
import AddOutlinedIcon from '@material-ui/icons/AddOutlined';
import CreateTreeModal from "./components/createTreeModal";

const useStyles = makeStyles((theme) => ({
    landingContainer: {
        padding: theme.spacing(2),
    },
    cardsDiv: {
        display: 'flex',
        flexWrap: 'wrap',
    },
    createButton: {
        textAlign: 'center',
    }
})); 

const Landing = ({dispatch, trees, user}) => {

    const [modalIsOpen, setModalIsOpen] = useState(false);
    const classes = useStyles();
    const cookies = new Cookies();
    const token = cookies.get('token');

    useEffect(() => {
        if (user.isAuthenticated){
            Axios.get(urls.myTrees, {
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${token}`
                }
            }).then( res => {
                const myTreeAdmin = res.data?.data?.admin?.map(tree => ({
                    title: tree.title,
                    id: tree.id,
                    isAdmin: true,
                }));
                const myTreeMember = res.data?.data?.member?.map(tree => ({
                    title: tree.title,
                    id: tree.id,
                    isAdmin: false,
                }));
                dispatch({
                    type: 'SET-MY-TREES',
                    data: {
                        myTrees:[...myTreeAdmin, ...myTreeMember],
                    }
                })
            }).catch( err => {
                toast.error(err.message);
            });

            Axios.get(urls.getAllTrees, {
                headers: {
                    Accept: 'application/json',
                    Authorization: `Bearer ${token}`
                }
            }).then( res => {
                const listOfTrees = res?.data?.data?.data?.map( tree => ({
                    title: tree.title,
                    id: tree.id,
                }));
                dispatch({
                    type: 'SET-TREESLIST',
                    data: {
                        treesList: listOfTrees,
                    }
                })
            }).catch( err => {
                toast.error(err.message);
            })
        }
    }, [user.isAuthenticated]);

    return (
        <div className={classes.landingContainer}>
            <div className={classes.createButton}>
                <Button color="primary" variant="contained" onClick={() => setModalIsOpen(true)}>
                    ایجاد شجره نامه
                    <AddOutlinedIcon/>
                </Button>
            </div>
            {
                trees.myTrees?.length > 0 ?
                    <Fragment>
                        <h4>شجره نامه های من</h4>
                        <Grid container>
                            {
                                trees.myTrees?.map((tree,index) =>
                                    <Grid item xs={2} key={index}>
                                        <TreeCard
                                            title={tree?.title}
                                            id={tree?.id}
                                            isMine={true}
                                            isAdmin={tree?.isAdmin}/>
                                    </Grid>)
                            }
                        </Grid>
                    </Fragment>
                    :
                    null

            }
            <h4 style={{marginTop: '1.5rem'}}>تمام شجره نامه ها</h4>
            <Grid container>
                {
                    trees.treesList?.length > 0 ? trees.treesList?.map((tree,index) =>
                        <Grid item xs={2} key={index}>
                            <TreeCard
                                title={tree?.title}
                                id={tree?.id}
                            />
                        </Grid>)
                        :
                        null
                }
            </Grid>
            <CreateTreeModal isOpen={modalIsOpen} handleClose={() => setModalIsOpen(false)}/>
        </div>
    )
};

const mapStateToProps = state => ({
    trees: state.tree,
    user: state.user,
});

export default connect(mapStateToProps)(Landing);