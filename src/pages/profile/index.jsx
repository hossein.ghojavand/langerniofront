import { useState, useEffect } from "react";
import {connect} from "react-redux";
import { makeStyles } from "@material-ui/core/styles";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import Cookies from "universal-cookie";
import { toast } from 'react-toastify';
import Grid from "@material-ui/core/Grid";
import AccountCircleOutlinedIcon from "@material-ui/icons/AccountCircleOutlined";
import Axios from "axios";
import {urls} from "../../utils/urls";


const useStyles = makeStyles((theme) => ({
  profilePage: {
    display: "flex",
    justifyContent: "center",
    alignItems: "center",
    width: "100%",
    height: "100%",
  },
  profileContainer: {
    position: "relative",
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    justifyContent: "space-between",
    width: "50%",
    minWidth: '30rem',
    height: "50%",
    minHeight: "24rem",
    border: `1px solid ${theme.palette.primary.main}`,
    borderRadius: "1rem",
    padding: "2rem",
    boxShadow: `0 0 20px ${theme.palette.primary.main}`,
  },
  userIcon: {
    fontSize: '8rem',
    color: theme.palette.primary.main,
  },
  profileRow: {
    textAlign: "left",
    width: "100%",
  },
  nameInput: {
    width: "100%",
    marginRight: "1rem",
    marginBottom: "1rem"
  },
  submitButton: {
    color: "white",
  },
  homeButton: {
    position: 'absolute',
    right: '1rem',
    top: '1rem',
  }
}));

const Profile = ({dispatch, user}) => {
  const classes = useStyles();
  const [userName, setUserName] = useState('');
  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [fatherName, setFatherName] = useState('');
  const [motherName, setMotherName] = useState('');
  const [phoneNumber, setPhoneNumber] = useState('');
  const cookies = new Cookies();
  const token = cookies.get('token');

  useEffect(() => {
    if(user.isAuthenticated) {
      Axios.get(urls.profile, {
        headers: {
          Accept: 'application/json',
          Authorization: `Bearer ${token}`
        }
      }).then(res => {
        setUserName(res?.data?.data?.name);
        setFirstName(res?.data?.data?.first_name);
        setLastName(res?.data?.data?.last_name);
        setFatherName(res?.data?.data?.father_name);
        setMotherName(res?.data?.data?.mother_name);
        setPhoneNumber(res?.data?.data?.phone_number);
      }).catch(err => {
        toast.error(err.message);
      })
    }
  }, []);

  const submit = (e) => {
    e.preventDefault();
    if(user.isAuthenticated) {
      const data = {
        name: userName,
        first_name: firstName,
        last_name: lastName,
        father_name: fatherName,
        mother_name: motherName,
        phone_number: phoneNumber,
      };
      Axios.post(urls.editProfile, data, {
        headers: {
          Accept: 'application/json',
          Authorization: `Bearer ${token}`
        }
      }).then(res => {
        dispatch({
          type: 'SET-USERNAME',
          data: {
            userName: userName,
          }
        });
        toast.success('اطلاعات حساب شما با موفقیت ذخیره شد');
      }).catch(err => {
        toast.error('ذخیره اطلاعات با مشکل مواجه شد');
      })
    }
  };

  return (
    <div className={classes.profilePage}>
      <form
        className={classes.profileContainer}
        onSubmit={(event) => submit(event)}
      >
        <AccountCircleOutlinedIcon className={classes.userIcon}/>
        <Grid container spacing={3}>
          <Grid item xs={3}>
            <TextField
                className={classes.nameInput}
                required
                id="outlined-required"
                label="نام کاربری"
                value={userName}
                onChange={(e) => setUserName(e.target.value)}
            />
          </Grid>
          <Grid item xs={9}/>
          <Grid item xs={3}>
            <TextField
                className={classes.nameInput}
                id="outlined-required"
                label="نام"
                value={firstName}
                onChange={(e) => setFirstName(e.target.value)}
            />
          </Grid>
          <Grid item xs={3}>
            <TextField
                className={classes.nameInput}
                id="outlined-required"
                label="نام خانوادگی"
                value={lastName}
                onChange={(e) => setLastName(e.target.value)}
            />
          </Grid>
          <Grid item xs={3}>
            <TextField
                className={classes.nameInput}
                id="outlined-required"
                label="نام پدر"
                value={fatherName}
                onChange={(e) => setFatherName(e.target.value)}
            />
          </Grid>
          <Grid item xs={3}>
            <TextField
                className={classes.nameInput}
                id="outlined-required"
                label="نام مادر"
                value={motherName}
                onChange={(e) => setMotherName(e.target.value)}
            />
          </Grid>
          <Grid item xs={12}>
            <TextField
                className={classes.nameInput}
                id="outlined-required"
                label="شماره موبایل"
                value={phoneNumber}
                onChange={(e) => setPhoneNumber(e.target.value)}
            />
          </Grid>
        </Grid>
        <div>
          <Button
            variant="contained"
            color="primary"
            type="submit"
            className={classes.submitButton}
          >
            ذخیره
          </Button>
        </div>
      </form>
    </div>
  );
};

const mapStateToProps = state => ({
  user: state.user
});

export default connect(mapStateToProps)(Profile);
