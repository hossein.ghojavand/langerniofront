import React, {useState} from 'react';
import {connect} from 'react-redux';
import {useHistory} from 'react-router-dom';
import Axios from "axios";
import {urls} from "../../utils/urls";
import {toast} from "react-toastify";
import Cookies from "universal-cookie";


// import Material UI component
import {makeStyles} from "@material-ui/core/styles";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import InputLabel from "@material-ui/core/InputLabel";
import Input from "@material-ui/core/Input";
import InputAdornment from "@material-ui/core/InputAdornment";
import IconButton from "@material-ui/core/IconButton";
import Visibility from "@material-ui/icons/Visibility";
import VisibilityOff from "@material-ui/icons/VisibilityOff";
import FormControl from "@material-ui/core/FormControl";

const useStyles = makeStyles((theme) => ({
    signInContainer: {
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    signInContent: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
        boxShadow: `0 0 10px ${theme.palette.primary.main}`,
        borderRadius: theme.spacing(2),
        width: '40%',
        height: '40%',
        minHeight: '15rem',
        padding: theme.spacing(2),
        boxSizing: 'border-box'
    },
    inputsDiv: {
        width: '100%',
        textAlign: 'center',
    },
    input: {
        textAlign: 'right',
        width: '70%',
        marginBottom: '1rem',
    },
    email: {

    }

}));

const SignIn = ({dispatch, user}) => {

    const classes = useStyles();
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState({
        text: '',
        showPassword: false,
    });
    const cookies = new Cookies();
    const history = useHistory();

    const submitHandler = event => {
        event.preventDefault();
        const data = {
            email,
            password: password.text,
        };
        Axios.post(urls.login, data).then(res => {
            cookies.set('token', res.data.token, { path: '/', maxAge: 10000000});
            dispatch({
                type: 'LOGIN',
            });
            history.push('/');
            toast.success('ورود شما با موفقیت انجام شد');
        }).catch(err => {
            toast.error('ورود شما با مشکل مواجه شد');
        })
    };

    return (
        <div className={classes.signInContainer}>
            <form className={classes.signInContent} onSubmit={submitHandler}>
                <h2>ورود</h2>
                <div className={classes.inputsDiv}>
                    <TextField
                        className={classes.input}
                        required
                        id="filled-required"
                        label="ایمیل"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                    />
                    <FormControl className={classes.input}>
                        <InputLabel htmlFor="standard-adornment-password">رمز عبور*</InputLabel>
                        <Input
                            id="standard-adornment-password"
                            type={password.showPassword ? 'text' : 'password'}
                            value={password.text}
                            required
                            onChange={e => setPassword({text: e.target.value, showPassword: password.showPassword})}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={e => setPassword({text: password.text, showPassword: !password.showPassword})}
                                        onMouseDown={e => e.preventDefault()}
                                    >
                                        {password.showPassword ? <Visibility /> : <VisibilityOff />}
                                    </IconButton>
                                </InputAdornment>
                            }
                        />
                    </FormControl>
                </div>
                <Button variant="contained" color="primary" type="submit">
                    ورود
                </Button>
            </form>
        </div>
    )
};

const mapStateToProps = state => ({
    user: state.user,
});

export default connect(mapStateToProps)(SignIn);