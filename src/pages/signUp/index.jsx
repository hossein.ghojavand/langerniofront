import React, {useState} from 'react';
import {connect} from 'react-redux';
import {useHistory} from 'react-router-dom';
import Axios from 'axios';
import {urls} from "../../utils/urls";
import { toast } from 'react-toastify';
import Cookies from 'universal-cookie';

// import Material UI component
import {makeStyles} from "@material-ui/core/styles";
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import Input from '@material-ui/core/Input';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import InputAdornment from '@material-ui/core/InputAdornment';
import Visibility from '@material-ui/icons/Visibility';
import VisibilityOff from '@material-ui/icons/VisibilityOff';

const useStyles = makeStyles((theme) => ({
    signInContainer: {
        width: '100%',
        height: '100%',
        display: 'flex',
        justifyContent: 'center',
        alignItems: 'center',
    },
    signInContent: {
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-between',
        alignItems: 'center',
        boxShadow: `0 0 10px ${theme.palette.primary.main}`,
        borderRadius: theme.spacing(2),
        width: '40%',
        height: '50%',
        minHeight: '18rem',
        padding: theme.spacing(2),
        boxSizing: 'border-box'
    },
    inputsDiv: {
        width: '100%',
        textAlign: 'center',
    },
    input: {
        textAlign: 'right',
        width: '70%',
        marginBottom: '1rem',
    },
    email: {

    }

}));

const SignUp = ({dispatch, user}) => {

    const classes = useStyles();
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState({
        text: '',
        showPassword: false,
    });
    const cookies = new Cookies();
    const history = useHistory();

    const submitHandler = event => {
        event.preventDefault();
        const data = {
            name,
            email,
            password: password.text,
        };
        if (password.text.length > 7) {
            Axios.post(urls.register, data).then(res => {
                cookies.set('token', res.data.token, { path: '/', maxAge: 10000000});
                dispatch({
                    type: 'LOGIN',
                });
                history.push('/');
                toast.success('ثبت نام شما با موفقیت انجام شد');
            }).catch(err => {
                toast.error('ثبت نام شما با مشکل مواجه شد');
            })
        } else {
            toast.error('!رمز عبور نباید کمتر از 8 حرف باشد')
        }

    };

    return (
        <div className={classes.signInContainer}>
            <form className={classes.signInContent} onSubmit={submitHandler}>
                <h2>ثبت نام</h2>
                <div className={classes.inputsDiv}>
                    <TextField
                        className={classes.input}
                        required
                        id="filled-required"
                        label="نام کاربری"
                        value={name}
                        onChange={e => setName(e.target.value)}
                    />
                    <TextField
                        className={classes.input}
                        required
                        id="filled-required"
                        label="ایمیل"
                        value={email}
                        onChange={e => setEmail(e.target.value)}
                    />
                    <FormControl className={classes.input}>
                        <InputLabel htmlFor="standard-adornment-password">رمز عبور*</InputLabel>
                        <Input
                            id="standard-adornment-password"
                            type={password.showPassword ? 'text' : 'password'}
                            value={password.text}
                            required
                            onChange={e => setPassword({text: e.target.value, showPassword: password.showPassword})}
                            endAdornment={
                                <InputAdornment position="end">
                                    <IconButton
                                        aria-label="toggle password visibility"
                                        onClick={e => setPassword({text: password.text, showPassword: !password.showPassword})}
                                        onMouseDown={e => e.preventDefault()}
                                    >
                                        {password.showPassword ? <Visibility /> : <VisibilityOff />}
                                    </IconButton>
                                </InputAdornment>
                            }
                        />
                    </FormControl>
                </div>
                <Button variant="contained" color="primary" type="submit">
                    ثبت نام
                </Button>
            </form>
        </div>
    )
};

const mapStateToProps = state => ({
    user: state.user,
});

export default connect(mapStateToProps)(SignUp);