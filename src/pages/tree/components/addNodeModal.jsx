import React, {useEffect, useState} from "react";
import Axios from "axios";
import Cookies from "universal-cookie";
import {toast} from "react-toastify";
import {makeStyles} from "@material-ui/core/styles";
import Modal from "@material-ui/core/Modal";
import TextField from "@material-ui/core/TextField";
import Button from "@material-ui/core/Button";
import {urls} from "../../../utils/urls";

const useStyles = makeStyles((theme) => ({
    modalBody: {
        position: "absolute",
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
        width: "40%",
        height: "fit-content",
        backgroundColor: theme.palette.background.paper,
        border: "none",
        borderRadius: theme.spacing(2),
        outline: "none",
        padding: theme.spacing(2),
        top: "50%",
        left: "50%",
        transform: `translate(-50%, -50%)`,
    },
    titleInput: {
        direction: 'ltr',
        marginBottom: theme.spacing(2),
        width: "100%",
    },
    descriptionInput: {
        direction: 'ltr',
        marginBottom: theme.spacing(2),
        width: "100%",
    },
    buttonsDiv: {
        display: "flex",
        justifyContent: "space-around",
        width: "50%",
    },
}));

const AddNodeModal = ({parentId, treeId, isOpen, handleClose}) => {
    const classes = useStyles();
    const cookies = new Cookies();
    const token = cookies.get('token');
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');


    const submitHandler = event => {
        event.preventDefault();
        let data;
        if (parseInt) {
            data = {
                family_tree_id: treeId,
                parent_id: parentId,
                first_name: firstName,
                last_name: lastName,
            };
        } else {
            data = {
                family_tree_id: treeId,
                first_name: firstName,
                last_name: lastName,
            };
        }
        Axios.post(urls.addUser, data, {
            headers: {
                Accept: 'application/json',
                Authorization: `Bearer ${token}`
            }
        }).then(res => {
            window.location.reload();
            handleClose();
            toast.success('عضو جدید با موفقیت به شجره نامه اضافه شد')
        }).catch(err => {
            toast.error('ایجاد عضو جدید با مشکل مواجه شد');
        })
    };

    return (
        <Modal
            open={isOpen}
            onClose={handleClose}
            aria-labelledby="simple-modal-title"
            aria-describedby="simple-modal-description"
        >
            <form className={classes.modalBody} onSubmit={submitHandler}>
              <h4>ایجاد عضو جدید</h4>
                <TextField
                    className={classes.titleInput}
                    required
                    id="filled-required"
                    label="نام"
                    value={firstName}
                    onChange={(e) => setFirstName(e.target.value)}
                />
                <TextField
                    className={classes.descriptionInput}
                    id="outlined-multiline-static"
                    required
                    label="نام خانوادگی"
                    value={lastName}
                    onChange={(e) => setLastName(e.target.value)}
                />
                <div className={classes.buttonsDiv}>
                    <Button variant="contained" type="submit" color="primary">
                        ایجاد
                    </Button>
                </div>
            </form>
        </Modal>
    );
};

export default AddNodeModal;
