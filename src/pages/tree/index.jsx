import React, { useState, useEffect} from 'react';
import {connect} from "react-redux";
import Axios from "axios";
import {urls} from "../../utils/urls";
import {toast} from "react-toastify";
import Cookies from "universal-cookie";
import Tree from 'react-d3-tree';
import { useCenteredTree } from "./components/centerTree";

// import Material UI components
import { makeStyles } from '@material-ui/core/styles';
import Drawer from '@material-ui/core/Drawer';
import List from '@material-ui/core/List';
import Divider from '@material-ui/core/Divider';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import AddNodeModal from "./components/addNodeModal";
import AccountCircleOutlinedIcon from "@material-ui/icons/AccountCircleOutlined";
import Button from "@material-ui/core/Button";
import AddOutlinedIcon from "@material-ui/icons/AddOutlined";

const useStyles = makeStyles((theme) => ({
    root: {
        display: 'flex',
    },
    drawer: {
        width: 240,
        zIndex: 0,
        flexShrink: 0,
        height: 'calc(100vh - 8rem)',
    },
    drawerContainer: {
        height: 'calc(100vh - 8rem)',
        textAlign: 'center',
    },
    drawerPaper: {
        position: 'static',
        backgroundColor: theme.palette.primary.dark,
        color: '#fff'
    },
    treeContainer: {
        flexGrow: 1,
        direction: 'rtl',
        textAlign: 'center',
        padding: theme.spacing(2)
    },
}));

const TreePage = ({user}) => {

    const [tree, setTree] = useState(undefined);
    const [nodesList, setNodesList] = useState(undefined);
    const [userIdSelected, setUserIdSelected] = useState(null);
    const [addModalIsOpen, setAddModalIsOpen] = useState(false);
    const [treeTitle, setTreeTitle] = useState('');
    const [editable, setEditable] = useState(false);
    const classes = useStyles();
    const cookies = new Cookies();
    const token = cookies.get('token');
    const treeId = localStorage.getItem('treeId');

    useEffect(() => {
        const data = {
            family_tree_id : treeId,
        };
        Axios.post(urls.getTree, data, {
            headers: {
                Accept: 'application/json',
                Authorization: `Bearer ${token}`
            }
        }).then( res => {
            setTreeTitle(res?.data?.data?.title);
            const tempTree = res?.data?.data?.nodes;
            setEditable(res?.data?.data?.admin?.email === user.userEmail);
            if(tempTree.id) {
                setTree(tempTree);
                setNodesList(renderListOfNode(tempTree).reverse());
            }
        }).catch( err => {
            toast.error(err.message);
        })
    },[]);
    
    const renderListOfNode = (node) => {
        const tempList = [];
        if(node.children.length > 0) {
            node.children.forEach(child => tempList.push(...renderListOfNode(child)));
            const thisNode = {
                firstName: node?.first_name,
                lastName: node?.last_name,
                userId: node?.user_id,
            };
            return [...tempList, thisNode];
        }
        else {
            return [{
                firstName: node?.first_name,
                lastName: node?.last_name,
                userId: node?.user_id,
            }]
        }
    };

    const handleNodeClick = (userId) => {
        // if(editable) {
            console.log(userId)

            setUserIdSelected(userId);
            setAddModalIsOpen(true);
        // }
    };

    const node = ({ nodeDatum, toggleNode, handleNodeClick } ) => {
        return (
            <g>
                <circle r="15" onClick={() => handleNodeClick(nodeDatum?.id)}  />

                <text fill="black" strokeWidth="1" x="20" onClick={toggleNode}>
                    {`${nodeDatum.first_name} ${nodeDatum.last_name}`}
                </text>
            </g>
        )
    };

    const [translate, containerRef] = useCenteredTree();

    return (
        <div className={classes.root}>
            <Drawer
                className={classes.drawer}
                variant="permanent"
                classes={{
                    paper: classes.drawerPaper,
                }}
            >
                <div className={classes.drawerContainer}>
                    <h3 style={{marginTop: '1rem'}}>اعضای شجره نامه</h3>
                    <List>
                        {nodesList?.map((user, index) => (
                            <ListItem button key={index}>
                                <ListItemIcon>
                                    <AccountCircleOutlinedIcon style={{color: '#fff'}}/>
                                </ListItemIcon>
                                <ListItemText primary={`${user.firstName} ${user.lastName}`} />
                            </ListItem>
                        ))}
                    </List>
                </div>
            </Drawer>

            <main className={classes.treeContainer} ref={containerRef}>
                <h3 style={{marginBottom: '1rem'}}>{treeTitle}</h3>
                {
                    tree !== undefined ?
                        <Tree
                            renderCustomNodeElement={(rd3tProps) =>
                                node({ ...rd3tProps, handleNodeClick })
                            }
                            translate={translate}
                            data={tree}
                            orientation='vertical'
                        />
                        :
                        editable ?
                            <Button variant="contained" color="primary" onClick={()=>setAddModalIsOpen(true)}>
                                <AddOutlinedIcon/>
                                ایجاد ریشه
                            </Button>
                            :
                            null
                }
            </main>
            <AddNodeModal
                parentId={userIdSelected}
                treeId={treeId}
                isOpen={addModalIsOpen}
                handleClose={() => setAddModalIsOpen(false)}/>
        </div>
    )
};

const mapStateToProps = state => ({
    user: state.user,
});

export default connect(mapStateToProps)(TreePage);