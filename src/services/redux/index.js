import { combineReducers } from 'redux';
import tree from './tree';
import user from './user';

const Index = combineReducers({
    tree,
    user,
});

export default Index;