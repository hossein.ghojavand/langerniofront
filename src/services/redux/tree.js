const initialState = {
    treesList: [],
    myTrees: [],
};

const TreeReducer =(state=initialState, action) => {
    switch (action.type){
        case 'SET-MY-TREES':
            return {
                ...state,
                myTrees: action.data.myTrees
            };
        case 'SET-TREESLIST':
            return {
                ...state,
                treesList: action.data.treesList
            };
        case 'ADD-MY-TREES':
            return {
                ...state,
                myTrees: [...state.myTrees, action.data.tree]
            };
        default:
            return state;
    }
};

export default TreeReducer;
