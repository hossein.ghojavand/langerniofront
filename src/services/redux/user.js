const initialState = {
    isAuthenticated: false,
    userName: '',
    userEmail: '',
};

const UserReducer =(state=initialState, action) => {
    switch (action.type){
        case "LOGIN":
            return {
                ...state,
                isAuthenticated: true,
            };
        case "SET-USER":
            return {
                ...state,
                userName: action.data.userName,
                userEmail: action.data.userEmail,
            };
        case "SET-USERNAME":
            return {
                ...state,
                userName: action.data.userName,
            };
        default:
            return state;
    }
};

export default UserReducer;
