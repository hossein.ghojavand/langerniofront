import { createMuiTheme } from "@material-ui/core/styles";

export const theme = createMuiTheme({
  direction: "rtl",
  palette: {
    primary: {
      main: "#1DD1A1",
      dark: "#0A7A5C",
      contrastText: '#fff',
    },
    secondary: {
      main: "#0E9F9F",
      contrastText: '#fff',
    },
    darkGray: "#555",
  },
  typography: {
    fontFamily: ["Vazir"],
  },
});
