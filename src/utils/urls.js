const baseUrl = "http://dally0027.ir:8080/api";

export const urls = {
    register: `${baseUrl}/register`,
    login: `${baseUrl}/login`,
    profile: `${baseUrl}/profile`,
    editProfile: `${baseUrl}/profile/edit`,
    myTrees: `${baseUrl}/profile/my_family_trees`,
    createTree: `${baseUrl}/family_tree/create`,
    getTree: `${baseUrl}/family_tree/show`,
    getAllTrees: `${baseUrl}/family_tree/index`,
    addUser: `${baseUrl}/family_tree/add_new_node`,
};